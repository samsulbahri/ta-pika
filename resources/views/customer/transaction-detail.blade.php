@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp

@extends('base.root-customer')

@section('css')
    <style>
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

        fieldset,
        label {
            margin: 0;
            padding: 0;
        }

        body {
            margin: 20px;
        }

        h1 {
            font-size: 1.5em;
            margin: 10px;
        }

        /****** Style Star Rating Widget *****/

        .rating {
            border: none;
            float: left;
        }

        .rating>input {
            display: none;
        }

        .rating>label:before {
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating>.half:before {
            content: "\f089";
            position: absolute;
        }

        .rating>label {
            color: #ddd;
            float: right;
        }

        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating>input:checked~label,
        /* show gold star when clicked */
        .rating:not(:checked)>label:hover,
        /* hover current star */
        .rating:not(:checked)>label:hover~label {
            color: #FFD700;
        }

        /* hover previous stars in list */

        .rating>input:checked+label:hover,
        /* hover current star when changing rating */
        .rating>input:checked~label:hover,
        .rating>label:hover~input:checked~label,
        /* lighten current selection */
        .rating>input:checked~label:hover~label {
            color: #FFED85;
        }
    </style>
@endsection
@section('main')
    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Terimakasih</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--================ confirmation part start =================-->
        <section class="confirmation_part section_padding">
            <div class="container">
                @if (session('success'))
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="confirmation_tittle">
                            <span>Halo kak, pesanan anda telah kami terima.</span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lx-4">
                        <div class="single_confirmation_details">
                            <h4>Pesanan</h4>
                            <ul>
                                <li>
                                    <p>Invoice</p><span>: {{ $order->invoice }}</span>
                                </li>
                                <li>
                                    <p>Status</p>
                                    <span>: <b>
                                            @if ($order->status == 'ordered')
                                                Dipesan
                                            @elseif ($order->status == 'fail_pay')
                                                Gagal Dibayar
                                            @elseif ($order->status == 'fail_shipping')
                                                Gagal dalam Pengiriman
                                            @elseif ($order->status == 'payed')
                                                Dibayar
                                            @elseif ($order->status == 'done')
                                                Selesai
                                            @endif
                                        </b>
                                    </span>
                                </li>
                                <li>
                                    <p>Total</p><span>: {{ rupiah($order->total_price) }}</span>
                                </li>
                                <li>
                                    <p>Metode Pembayaran</p><span>: {{ $order->tripay_channel->channel_name }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lx-4">
                        <div class="single_confirmation_details">
                            <h4>Alamat</h4>
                            <ul>
                                <li>
                                    <p>Detail Alamat</p><span>: {{ $order->customer->customer_address }}</span>
                                </li>
                                <li>
                                    <p>Kota</p><span>: {{ $order->customer->customer_city }}</span>
                                </li>
                                <li>
                                    <p>Provinsi</p><span>: {{ $order->customer->customer_province }}</span>
                                </li>
                                <li>
                                    <p>Kode Pos</p><span>: {{ $order->customer->customer_zip_code }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="order_details_iner">
                            <h3>Detail Pesanan</h3>
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        @if ($order->status == 'done')
                                            <th scope="col">Review</th>
                                        @endif
                                        <th scope="col" colspan="2">Produk</th>
                                        <th scope="col">Jumlah</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($detailOrders as $detailOrder)
                                        <tr>
                                            @if ($order->status == 'done')
                                                <th>
                                                    @if ($detailOrder->rating_star == null)
                                                        <button class="genric-btn success radius" data-toggle="modal"
                                                            data-target="#ratingModal{{ $detailOrder->cart_id }}">Beri
                                                            Review</button>
                                                    @else
                                                        <button class="genric-btn success-border">Terimakasih</button>
                                                    @endif
                                                </th>
                                            @endif
                                            <th colspan="2"><span>{{ $detailOrder->product->product_name }}</span></th>
                                            <th>x {{ $detailOrder->quantity }}</th>
                                            <th> <span>{{ rupiah($detailOrder->quantity * $detailOrder->product->product_price) }}</span>
                                            </th>
                                        </tr>
                                        @php
                                            $total += $detailOrder->quantity * $detailOrder->product->product_price;
                                        @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        @if ($order->status == 'done')
                                            <th scope="col" colspan="4"></th>
                                        @else
                                            <th scope="col" colspan="3"></th>
                                        @endif
                                        <th scope="col">{{ rupiah($total) }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================ confirmation part end =================-->
    </main>

    @foreach ($detailOrders as $detailOrder)
        <div class="modal fade" id="ratingModal{{ $detailOrder->cart_id }}" tabindex="-1"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Beri Rating</h1>
                    </div>
                    <form action="/rating" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $detailOrder->cart_id }}">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col">
                                    <fieldset class="rating">
                                        <input type="radio" id="star5" name="rating" value="5" /><label
                                            class = "full" for="star5" title="Keren - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label
                                            class = "full" for="star4" title="Bagus - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label
                                            class = "full" for="star3" title="Standar - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label
                                            class = "full" for="star2" title="Perlu perbaikan - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label
                                            class = "full" for="star1" title="Sangat Buruk - 1 star"></label>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <textarea name="message" class="form-control" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="genric-btn primary small" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="genric-btn success small">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
