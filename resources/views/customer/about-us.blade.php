@extends('base.root-customer')
@section('main')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Tentang Kami</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!-- About Details Start -->
    <div class="about-details section-padding30">
        <div class="container">
            <div class="row">
                <div class="offset-xl-1 col-lg-8">
                    <div class="about-details-cap mb-50">
                        <h4>Visi Kami</h4>
                        <p>Mewujudkan keindahan alam Bali dalam setiap rangkaian bunga, menjadi penyemangat dan penambah kebahagiaan bagi setiap moment berharga dalam kehidupan pelanggan kami.</p>
                    </div>

                    <div class="about-details-cap mb-50">
                        <h4>Misi Kami</h4>
                        <p>Misi kami adalah memberikan kualitas bunga terbaik dengan desain yang unik, pelayanan pelanggan luar biasa, praktik bisnis yang berkelanjutan, dan kontribusi positif kepada komunitas lokal.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Details End -->
    <!--? Shop Method Start-->
    <div class="shop-method-area">
        <div class="container">
            <div class="method-wrapper">
                <div class="row d-flex justify-content-between">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-package"></i>
                            <h6>Metode Pengiriman Gratis</h6>
                            <p>Dengan Metode Pengiriman Gratis, Bunga Bali Florist menawarkan layanan pengiriman tanpa biaya tambahan untuk memastikan pelanggan dapat menikmati keindahan bunga-bunga segar tanpa harus khawatir tentang biaya pengiriman tambahan.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-unlock"></i>
                            <h6>Secure Payment System</h6>
                            <p>Sistem Pembayaran Aman di Bunga Bali Florist memberikan jaminan keamanan transaksi kepada pelanggan, sehingga mereka dapat melakukan pembayaran dengan percaya diri dan nyaman, tanpa risiko kebocoran data atau penyalahgunaan informasi pribadi.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-reload"></i>
                            <h6>Barang Kualitas Terbaik
                            </h6>
                            <p>Barang kualitas terbaik di Bunga Bali Florist adalah hasil dari seleksi teliti bunga-bunga segar yang dipilih dari kebun terbaik di Bali, menjamin bahwa setiap produk yang disediakan memenuhi standar tertinggi dalam hal keindahan, kesegaran, dan daya tahan.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Shop Method End-->
</main>
@endsection