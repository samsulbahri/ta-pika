@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp

@extends('base.root-customer')
@section('main')
    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>{{$categoryName}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End-->
        <!-- Latest Products Start -->
        <section class="popular-items latest-padding">
            <div class="container">
                <!-- Nav Card -->
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row">
                            @foreach ($products as $product)
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="single-popular-items mb-50 text-center">
                                    <div class="popular-img">
                                        <img src="{{$product->product_image_1}}" alt="{{$product->product_name}}">
                                    </div>
                                    <div class="popular-caption">
                                        <h3><a href="/detail-barang/{{strtolower(str_replace(' ', '_', $product->product_name))}}">{{$product->product_name}}</a></h3>
                                        <span>{{rupiah($product->product_price)}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- End Nav Card -->
            </div>
        </section>
        <!-- Latest Products End -->
        <!--? Shop Method Start-->
        <div class="shop-method-area">
            <div class="container">
                <div class="method-wrapper">
                    <div class="row d-flex justify-content-between">
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="single-method mb-40">
                                <i class="ti-package"></i>
                                <h6>Metode Pengiriman Gratis</h6>
                                <p>Dengan Metode Pengiriman Gratis, Bunga Bali Florist menawarkan layanan pengiriman tanpa biaya tambahan untuk memastikan pelanggan dapat menikmati keindahan bunga-bunga segar tanpa harus khawatir tentang biaya pengiriman tambahan.</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="single-method mb-40">
                                <i class="ti-unlock"></i>
                                <h6>Secure Payment System</h6>
                                <p>Sistem Pembayaran Aman di Bunga Bali Florist memberikan jaminan keamanan transaksi kepada pelanggan, sehingga mereka dapat melakukan pembayaran dengan percaya diri dan nyaman, tanpa risiko kebocoran data atau penyalahgunaan informasi pribadi.</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="single-method mb-40">
                                <i class="ti-reload"></i>
                                <h6>Barang Kualitas Terbaik
                                </h6>
                                <p>Barang kualitas terbaik di Bunga Bali Florist adalah hasil dari seleksi teliti bunga-bunga segar yang dipilih dari kebun terbaik di Bali, menjamin bahwa setiap produk yang disediakan memenuhi standar tertinggi dalam hal keindahan, kesegaran, dan daya tahan.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shop Method End-->
    </main>
@endsection
