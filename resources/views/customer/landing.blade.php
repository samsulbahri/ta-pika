@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp

@extends('base.root-customer')
@section('main')
    <main>
        <!--? slider Area Start -->
        <div class="slider-area ">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider slider-height d-flex align-items-center slide-bg">
                    <div class="container">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInLeft" data-delay=".4s" data-duration="2000ms" style="color: #31a8b5;">Bunga Bali Florist</h1>
                                    <p data-animation="fadeInLeft" data-delay=".7s" data-duration="2000ms">Mempercantik Hidup Anda dengan Keindahan Alami: <b>Bunga Bali Florist</b> - Tempat di Mana Setiap Bunga Menceritakan Kisah Kecantikan dan Kehangatan.</p>
                                    <!-- Hero-btn -->
                                    <div class="hero__btn" data-animation="fadeInLeft" data-delay=".8s"
                                        data-duration="2000ms">
                                        <a href="#new-product" class="btn hero-btn">Belanja Sekarang</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 d-none d-sm-block">
                                <div class="hero__img" data-animation="bounceIn" data-delay=".4s">
                                    <img src="assets/img/hero/watch.png" alt="" class=" heartbeat">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!-- ? New Product Start -->
        <section id="new-product" class="new-product-area section-padding30">
            <div class="container">
                <!-- Section tittle -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="section-tittle mb-70">
                            <h2>Barang Baru</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach ($newArrivals as $newArrival)
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                        <div class="single-new-pro mb-30 text-center">
                            <div class="product-img">
                                @php
                                    $randomPhoto = "product_image_". rand(1,3);
                                @endphp
                                <img src="{{$newArrival->$randomPhoto}}" alt="">
                            </div>
                            <div class="product-caption">
                                <h3><a href="/detail-barang/{{strtolower(str_replace(' ', '_', $newArrival->product_name))}}">{{$newArrival->product_name}}</a></h3>
                                <span>{{rupiah($newArrival->product_price)}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!--  New Product End -->
        <!--? Gallery Area Start -->
        <div class="gallery-area">
            <div class="container-fluid p-0 fix">
                <div class="row">
                    @php
                        $randomPhoto = "product_image_". rand(1,3);
                    @endphp
                    <div class="col-xl-6 col-lg-4 col-md-6 col-sm-6">
                        <div class="single-gallery mb-30">
                            <div class="gallery-img big-img"
                                style="background-image: url({{$newArrivals[rand(0,count($newArrivals)-1)]->$randomPhoto}});"></div>
                        </div>
                    </div>
                    @php
                        $randomPhoto = "product_image_". rand(1,3);
                    @endphp
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                        <div class="single-gallery mb-30">
                            <div class="gallery-img big-img"
                                style="background-image: url({{$newArrivals[rand(0,count($newArrivals)-1)]->$randomPhoto}});"></div>
                        </div>
                    </div>
                    
                    <div class="col-xl-3 col-lg-4 col-md-12">
                        <div class="row">
                            @php
                                $randomPhoto = "product_image_". rand(1,3);
                            @endphp
                            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-6">
                                <div class="single-gallery mb-30">
                                    <div class="gallery-img small-img"
                                        style="background-image: url({{$newArrivals[rand(0,count($newArrivals)-1)]->$randomPhoto}});"></div>
                                </div>
                            </div>
                            @php
                                $randomPhoto = "product_image_". rand(1,3);
                            @endphp
                            <div class="col-xl-12 col-lg-12  col-md-6 col-sm-6">
                                <div class="single-gallery mb-30">
                                    <div class="gallery-img small-img"
                                        style="background-image: url({{$newArrivals[rand(0,count($newArrivals)-1)]->$randomPhoto}});"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Gallery Area End -->
        <!--? Popular Items Start -->
        <div class="popular-items section-padding30">
            <div class="container">
                <!-- Section tittle -->
                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-8 col-md-10">
                        <div class="section-tittle mb-70 text-center">
                            <h2>Barang Terpopuler</h2>
                            <p>Bunga segar pilihan terbaik dari kebun terbaik di Bali, cocok untuk setiap momen istimewa, hanya di Bunga Bali Florist.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach ($hotProducts as $hotProduct)
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                        <div class="single-new-pro mb-30 text-center">
                            <div class="product-img">
                                <img src="{{$hotProduct->product_image_1}}" alt="">
                            </div>
                            <div class="product-caption">
                                <h3><a href="/detail-barang/{{strtolower(str_replace(' ', '_', $hotProduct->product_name))}}">{{$hotProduct->product_name}}</a></h3>
                                <span>{{rupiah($hotProduct->product_price)}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Popular Items End -->
        <!--? Shop Method Start-->
        <div class="shop-method-area">
            <div class="container">
                <div class="method-wrapper">
                    <div class="row d-flex justify-content-between">
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="single-method mb-40">
                                <i class="ti-package"></i>
                                <h6>Metode Pengiriman Gratis</h6>
                                <p>Dengan Metode Pengiriman Gratis, Bunga Bali Florist menawarkan layanan pengiriman tanpa biaya tambahan untuk memastikan pelanggan dapat menikmati keindahan bunga-bunga segar tanpa harus khawatir tentang biaya pengiriman tambahan.</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="single-method mb-40">
                                <i class="ti-unlock"></i>
                                <h6>Secure Payment System</h6>
                                <p>Sistem Pembayaran Aman di Bunga Bali Florist memberikan jaminan keamanan transaksi kepada pelanggan, sehingga mereka dapat melakukan pembayaran dengan percaya diri dan nyaman, tanpa risiko kebocoran data atau penyalahgunaan informasi pribadi.</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6">
                            <div class="single-method mb-40">
                                <i class="ti-reload"></i>
                                <h6>Barang Kualitas Terbaik
                                </h6>
                                <p>Barang kualitas terbaik di Bunga Bali Florist adalah hasil dari seleksi teliti bunga-bunga segar yang dipilih dari kebun terbaik di Bali, menjamin bahwa setiap produk yang disediakan memenuhi standar tertinggi dalam hal keindahan, kesegaran, dan daya tahan.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shop Method End-->
    </main>
@endsection
