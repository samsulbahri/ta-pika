@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp

@extends('base.root-customer')
@section('main')
    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>{{ $dbProduct->product_name }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End-->
        <!--================Single Product Area =================-->
        <div class="product_image_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="product_img_slide owl-carousel">
                            <div class="single_product_img">
                                <img src="{{ asset($dbProduct->product_image_1) }}" alt="#" class="img-fluid">
                            </div>
                            <div class="single_product_img">
                                <img src="{{ asset($dbProduct->product_image_2) }}" alt="#" class="img-fluid">
                            </div>
                            <div class="single_product_img">
                                <img src="{{ asset($dbProduct->product_image_3) }}" alt="#" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="single_product_text text-center">
                            <p>
                            </p>
                            @if ($dbProduct->product_quantity == 0)
                                <p class="text-danger font-weight-light font-italic">Stok Kosong</p>
                            @else
                            <div class="card_area">
                                <form action="/tambahkan-ke-keranjang" method="POST">
                                    @csrf
                                    <input type="hidden" name="product" value="{{ $dbProduct->product_id }}">
                                    <div class="product_count_area">
                                        <p>Quantity</p>
                                        <div class="product_count d-inline-block">
                                            <span class="product_count_item inumber-decrement"> <i
                                                    class="ti-minus"></i></span>
                                            <input class="product_count_item input-number" type="text" value="1"
                                                min="0" max="10" name="quantity">
                                            <span class="product_count_item number-increment"> <i
                                                    class="ti-plus"></i></span>
                                        </div>
                                        <p>{{ rupiah($dbProduct->product_price) }} / Item</p>
                                    </div>
                                    <div class="add_to_cart">
                                        <button type="submit" class="btn_3">Tambahkan ke Keranjang</button>
                                    </div>
                                </form>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (count($reviews) != 0)
        <div class="container">
            <h1>Ulasan</h1>
            <div class="row">
                @foreach ($reviews as $review)
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <b>{{$review->product->product_name}}</b>
                        </div>
                        <div class="card-body">
                            @if ($review->rating_message == null)
                                -
                            @else
                                {{$review->rating_message}}
                            @endif
                        </div>
                        <div class="card-footer">
                            <?php
                            $i = 0;
                            while($i < $review->rating_star) { ?>
                            <i class="bi bi-star-fill" style="font-size: 30px; color: #fd7e14;"></i>
                            <?php 
                            $i++;
                            }

                            $j = 5 - $review->rating_star;
                            while($j > 0) { ?>
                            <i class="bi bi-star" style="font-size: 30px; color: #fd7e14;"></i>
                            <?php 
                            $j--;
                            }
                            ?>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </main>

    <div class="modal fade" id="confirmationModal" tabindex="-1" aria-labelledby="confirmationModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmationModalLabel">Berhasil</h5>
                </div>
                <div class="modal-body" id="modal-body">
                    Barang telah ditambahkan ke keranjang anda
                </div>
                <div class="modal-footer">
                    <button type="button" class="genric-btn primary small" data-dismiss="modal">Baiklah</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @if (session('success'))
        <script type="text/javascript">
            $(window).on('load', function() {
                $('#confirmationModal').modal('show');
            });
        </script>
    @endif
@endsection
