@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp
@extends('base.root-customer')
@section('main')
    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Checkout</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--================Checkout Area =================-->
        <section class="checkout_area section_padding">
            <div class="container">
                <div class="billing_details">
                    <form class="row contact_form" action="/pesan" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-lg-8">
                                <h3>Informasi Pribadi</h3>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="customer_name"
                                        value="{{ $customer->customer_name }}" placeholder="Nama" required />
                                </div>
                                <div class="col-md-5 form-group p_star">
                                    <input type="text" class="form-control" id="text" name="customer_phone_number"
                                        value="{{ $customer->customer_phone_number }}" placeholder="Nomor Telpon"
                                        required />
                                </div>
                                <div class="col-md-5 form-group p_star">
                                    <input type="email" class="form-control" id="email" name="customer_email"
                                        value="{{ $customer->customer_email }}" placeholder="Email" required />
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" id="add1" name="customer_address"
                                        value="{{ $customer->customer_address }}" placeholder="Alamat" required />
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" id="city" name="customer_city"
                                        value="{{ $customer->customer_city }}" placeholder="Kota" required />
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" id="province" name="customer_province"
                                        value="{{ $customer->customer_province }}" placeholder="Provinsi" required />
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" id="zip" name="customer_zip_code"
                                        value="{{ $customer->customer_zip_code }}" placeholder="Kode Pos" required />
                                </div>
                                <div class="col-md-12 form-group">
                                    <div class="creat_account">
                                        <h3>Informasi Pengiriman</h3>
                                    </div>
                                    <textarea class="form-control" name="message" id="message" rows="1" placeholder="Catatan"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="order_box">
                                    <h2>Pesanan Anda</h2>
                                    <ul class="list">
                                        <li>
                                            <a href="#">Product
                                                <span>Total</span>
                                            </a>
                                        </li>
                                        @php
                                            $total = 0;
                                        @endphp
                                        @foreach ($carts as $cart)
                                            <li>
                                                <a href="#">{{ $cart->product->product_name }}
                                                    <span class="middle">x {{ $cart->quantity }}</span>
                                                    <span
                                                        class="last">{{ rupiah($cart->product->product_price * $cart->quantity) }}</span>
                                                </a>
                                            </li>
                                            @php
                                                $total += $cart->product->product_price * $cart->quantity;
                                            @endphp
                                        @endforeach
                                    </ul>
                                    <ul class="list list_2">
                                        <li>
                                            <a href="#">Total
                                                <span>{{ rupiah($total) }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="payment_item">
                                        <div class="radion_btn">
                                            <label for="f-option5" style="padding: 0px 25px 21px 0px;">Pilih
                                                Pembayaran</label>
                                        </div>
                                        <div class="accordion" id="accordionPayment">
                                            @foreach ($paymentGroups as $paymentGroup)
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header"
                                                        id="heading-{{ strtolower(str_replace(' ', '_', $paymentGroup->channel_group)) }}">
                                                        <button class="accordion-button collapsed" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse-{{ strtolower(str_replace(' ', '_', $paymentGroup->channel_group)) }}"
                                                            aria-expanded="false"
                                                            aria-controls="collapse-{{ strtolower(str_replace(' ', '_', $paymentGroup->channel_group)) }}"
                                                            style="color: #415094; border:none;">
                                                            {{ $paymentGroup->channel_group }}
                                                        </button>
                                                    </h2>
                                                    <div id="collapse-{{ strtolower(str_replace(' ', '_', $paymentGroup->channel_group)) }}"
                                                        class="accordion-collapse collapse"
                                                        aria-labelledby="heading{{ strtolower(str_replace(' ', '_', $paymentGroup->channel_group)) }}"
                                                        data-parent="#accordionPayment">
                                                        <div class="accordion-body">
                                                            @foreach ($paymentChannels as $paymentChannel)
                                                                @if ($paymentChannel->channel_group == $paymentGroup->channel_group)
                                                                    <div id="card-payment-{{ $paymentChannel->channel_id }}"
                                                                        class="card mb-3 card-payment"
                                                                        onclick="setPaymentActive('{{ $paymentChannel->channel_id }}', '{{ $paymentChannel->channel_name }}')">

                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col">
                                                                                    <img src="{{ $paymentChannel->channel_icon_url }}"
                                                                                        width="50"
                                                                                        alt="{{ $paymentChannel->channel_name }}"
                                                                                        class="float-start">
                                                                                    <span
                                                                                        id="text-payment-{{ $paymentChannel->channel_id }}"
                                                                                        class="float-right text-payment"
                                                                                        flat='{{ $paymentChannel->fee_customer_flat }}'
                                                                                        percent='{{ $paymentChannel->fee_customer_percent }}'
                                                                                        minimum='{{ $paymentChannel->minimum_fee }}'
                                                                                        maximum='{{ $paymentChannel->maximum_fee }}'
                                                                                        group='{{ $paymentChannel->channel_group }}'></span>
                                                                                </div>
                                                                            </div>
                                                                            <hr class="mt-1 mb-1">
                                                                            {{ $paymentChannel->channel_name }}
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input type="hidden" name="channel_id" id="channel">
                                    <button class="btn_3" type="submit">Proses Transaksi</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!--================End Checkout Area =================-->
    </main>
@endsection

@section('js')
<script>
    let priceSelected = {{$total}};

    function getPayment() {
            let channels = document.getElementsByClassName('text-payment')
            for (let i = 0; i < channels.length; i++) {
                let actualPrice = priceSelected;
                let percent = channels[i].getAttribute('percent');
                let flat = channels[i].getAttribute('flat');
                let minimum = channels[i].getAttribute('minimum');
                let maximum = channels[i].getAttribute('maximum');
                let group = channels[i].getAttribute('group');

                if (percent != 0) {
                    let tax = (((percent / 100).toFixed(3) * priceSelected) + parseInt(flat));
                    if (minimum != '') {
                        if (tax < parseInt(minimum)) {
                            tax = parseInt(minimum);
                        }
                    }

                    if (maximum != '') {
                        if (tax > parseInt(maximum)) {
                            tax = parseInt(maximum);
                        }
                    }

                    actualPrice = parseInt(priceSelected) + tax;
                } else {
                    actualPrice = parseInt(priceSelected) + parseInt(flat);
                }

                channels[i].innerHTML = "Rp. " + formatRupiah(Math.round(actualPrice).toString()) + ",00";

                if (group == 'Virtual Account' || group == 'Convenience Store') {
                    if (priceSelected < 10000) {
                        channels[i].innerHTML = "<i>Sedang tidak tersedia</i>";
                    }
                }
            }
        }

        function setPaymentActive(card, name) {
            switch (document.getElementById("text-payment-" + card).innerHTML) {
                case '':
                    alert("Silahkan memilih jasa terlebih dahulu");
                    return false;
                case '<i>Sedang tidak tersedia</i>':
                    alert("Metode pembayaran ini sedang tidak dapat digunakan");
                    return false;
                default:
                    let payments = document.getElementsByClassName('card-payment');
                    for (i = 0; i < payments.length; i++) {
                        payments[i].classList.remove('border-primary');
                    }

                    document.getElementById("card-payment-" + card).classList.add("border-primary");
                    document.getElementById('channel').value = card;

                    location.href = "#section-2";
            }

        }

        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        getPayment();
</script>
@endsection
