@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp
@extends('base.root-customer')
@section('main')
    <main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Keranjang</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--================Cart Area =================-->
        <section class="cart_area section_padding">
            <div class="container">
                <div class="cart_inner">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Produk</th>
                                    <th scope="col">Harga</th>
                                    <th scope="col">Jumlah</th>
                                    <th scope="col">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($carts) != 0)
                                    @php
                                        $subtotal = 0;
                                    @endphp
                                    <form method="post" action="/update-cart">
                                        @csrf
                                        @foreach ($carts as $cart)
                                            <tr>
                                                <td>
                                                    <div class="media">
                                                        <div class="d-flex">
                                                            <img src="{{ asset($cart->product->product_image_1) }}"
                                                                alt="" />
                                                        </div>
                                                        <div class="media-body">
                                                            <p>{{ $cart->product->product_name }}</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h5>{{ rupiah($cart->product->product_price) }}</h5>
                                                </td>
                                                <td>
                                                    <div class="product_count">
                                                        <span class="input-number-decrement"> <i
                                                                class="ti-minus"></i></span>
                                                        <input class="input-number-{{ $cart->cart_id }}" type="text"
                                                            name="quantity_{{ $cart->cart_id }}"
                                                            value="{{ $cart->quantity }}" min="0" max="10">
                                                        <span class="input-number-increment"> <i class="ti-plus"></i></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h5>{{ rupiah($cart->product->product_price * $cart->quantity) }}</h5>
                                                    @php
                                                        $subtotal += $cart->product->product_price * $cart->quantity;
                                                    @endphp
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr class="bottom_button">
                                            <td>
                                                <button class="btn_1" type="submit">Ubah Keranjang</a>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                            </td>
                                        </tr>
                                </form>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <h5>Subtotal</h5>
                                    </td>
                                    <td>
                                        <h5>{{ rupiah($subtotal) }}</h5>
                                    </td>
                                </tr>
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Belum ada barang pada keranjang</td>
                                    </tr>
                            @endif
                            </tbody>
                        </table>
                        <div class="checkout_btn_inner float-right">
                            <a class="btn_1" href="/">Lanjut Belanja</a>
                            @if(count($carts)!= 0)
                            <a class="btn_1 checkout_btn_1" href="/pesan">Checkout</a>
                            @endif
                        </div>
                    </div>
                </div>
        </section>
        <!--================End Cart Area =================-->
    </main>
@endsection

@section('js')
    <script>
        (function() {
            window.inputNumber = function(el) {

                var min = el.attr('min') || false;
                var max = el.attr('max') || false;

                var els = {};

                els.dec = el.prev();
                els.inc = el.next();

                el.each(function() {
                    init($(this));
                });

                function init(el) {

                    els.dec.on('click', decrement);
                    els.inc.on('click', increment);

                    function decrement() {
                        var value = el[0].value;
                        value--;
                        if (!min || value >= min) {
                            el[0].value = value;
                        }
                    }

                    function increment() {
                        var value = el[0].value;
                        value++;
                        if (!max || value <= max) {
                            el[0].value = value++;
                        }
                    }
                }
            }
        })();

        @foreach ($carts as $cart)
            inputNumber($('.input-number-{{ $cart->cart_id }}'));
        @endforeach
    </script>
@endsection
