
@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp
@extends('base.root-manager')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap4.min.css">
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap4.min.js"></script>

<script>
    new DataTable('#table');
</script>
@endsection

@section('main')
    <!--**********************************
                    Content body start
                ***********************************-->
    <div class="content-body">
        <div class="container-fluid mt-3">

            @if (session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session('error') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="active-member">
                                <div class="table-responsive">
                                    <table class="table table-xs mb-0" id="table">
                                        <thead>
                                            <tr>
                                                <th>Invoice</th>
                                                <th>Pembeli</th>
                                                <th>Pembayaran</th>
                                                <th>Total Harga</th>
                                                <th>Status Pembelian</th>
                                                <th>Link Pembayaran</th>
                                                <th>Tanggal Transaksi</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($detailOrders as $detailOrder)
                                            <tr>
                                                <td>#{{$detailOrder->invoice}}</td>
                                                <td>{{$detailOrder->customer_name}}</td>
                                                <td>{{$detailOrder->channel_name}}</td>
                                                <td>{{rupiah($detailOrder->total_price)}}</td>
                                                @if ($detailOrder->status == 'ordered')
                                                <td><span class="badge badge-primary">Dipesan</span></td>
                                                @elseif ($detailOrder->status == 'fail_pay')
                                                <td><span class="badge badge-danger">Gagal Membayar</span></td>
                                                @elseif ($detailOrder->status == 'fail_shipping')
                                                <td><span class="badge badge-danger">Gagal Pengiriman</span></td>
                                                @elseif ($detailOrder->status == 'payed')
                                                <td><span class="badge badge-info">Dibayar</span></td>
                                                @elseif ($detailOrder->status == 'done')
                                                <td><span class="badge badge-success">Selesai</span></td>
                                                @endif
                                                <td><a target="__blank" href="{{$detailOrder->payment_url}}">{{$detailOrder->payment_url}}</a></td>
                                                <td>{{$detailOrder->order_date}}</td>
                                                @if ($detailOrder->status == 'ordered')
                                                <td><a href="/staff/cek-pembayaran/{{$detailOrder->invoice}}" class="btn btn-primary">Cek Pembayaran</a></td>
                                                @elseif ($detailOrder->status == 'fail_pay')
                                                <td><a target="__blank" href="https://wa.me/{{$detailOrder->customer_phone_number}}" class="btn btn-warning">Hubungi Customer</a></td> 
                                                @elseif ($detailOrder->status == 'fail_shipping')
                                                <td><a target="__blank" href="https://wa.me/6281277057373" class="btn btn-danger">Hubungi Manajer</a></td>
                                                @elseif ($detailOrder->status == 'payed')
                                                <td><a target="__blank" href="https://wa.me/{{$detailOrder->customer_phone_number}}" class="btn btn-info">Hubungi Customer</a></td> 
                                                <td><a href="/staff/order-selesai/{{$detailOrder->order_id}}" class="btn btn-success">Selesaikan Pesanan</a></td>
                                                @elseif ($detailOrder->status == 'done')
                                                <td class="text-center">-</td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Export Data</h5> <button type="button" class="close"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="/manager/export-penjualan" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Download</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- #/ container -->
    </div>
    <!--**********************************
                    Content body end
                ***********************************-->
@endsection
