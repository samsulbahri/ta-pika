@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp
@extends('base.root-staff')
@section('main')
    <!--**********************************
                Content body start
            ***********************************-->
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-1">
                        <div class="card-body">
                            <h3 class="card-title text-white">Produk Terjual</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white">{{$totalOrder->quantity}}</h2>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-shopping-cart"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-2">
                        <div class="card-body">
                            <h3 class="card-title text-white">Penghasilan</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white">{{rupiah($totalRev->quantity)}}</h2>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-3">
                        <div class="card-body">
                            <h3 class="card-title text-white">Total Customer</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white">{{$totalCustomer}}</h2>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-4">
                        <div class="card-body">
                            <h3 class="card-title text-white">Total Ulasan</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white">{{$totalReview}}</h2>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-heart"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body pb-0 d-flex justify-content-between">
                                    <div>
                                        <h4 class="mb-1">Pendapatan Bulan Ini</h4>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <canvas id="chart_widget_2"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="active-member">
                                <div class="table-responsive">
                                    <table class="table table-xs mb-0">
                                        <thead>
                                            <tr>
                                                <th>Customer</th>
                                                <th>Invoice</th>
                                                <th>Status</th>
                                                <th>Metode Pembayaran</th>
                                                <th>Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($dataSaleCustomers as $dataSaleCustomer)
                                            <tr>
                                                <td>{{$dataSaleCustomer->customer->customer_name}}</td>
                                                <td>#{{$dataSaleCustomer->invoice}}</td>
                                                @if ($dataSaleCustomer->status == 'ordered')
                                                <td><span class="badge badge-primary">Dipesan</span></td>
                                                @elseif ($dataSaleCustomer->status == 'fail_pay')
                                                <td><span class="badge badge-danger">Gagal Membayar</span></td>
                                                @elseif ($dataSaleCustomer->status == 'fail_shipping')
                                                <td><span class="badge badge-danger">Gagal Pengiriman</span></td>
                                                @elseif ($dataSaleCustomer->status == 'payed')
                                                <td><span class="badge badge-info">Dibayar</span></td>
                                                @elseif ($dataSaleCustomer->status == 'done')
                                                <td><span class="badge badge-success">Selesai</span></td>
                                                @endif
                                                <td>{{$dataSaleCustomer->tripay_channel->channel_name}}</td>
                                                <td>
                                                    <span class="m-0 pl-3">{{$dataSaleCustomer->order_date}}</span>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
                Content body end
            ***********************************-->
@endsection

@section('js')
<script>
    
(function($) {
    "use strict"

    let ctx = document.getElementById("chart_widget_2");
    ctx.height = 280;
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
            <?php
            foreach($dailyIncome as $income) {
                echo "'Hari ke-".$income->day."',";
            }
            ?>        
        ],
            type: 'line',
            defaultFontFamily: 'Montserrat',
            datasets: [{
                data: [
                    <?php
                    foreach($dailyIncome as $income) {
                        echo $income->income .",";
                    }
                    ?>   
                ],
                label: "Pendapatan",
                backgroundColor: '#847DFA',
                borderColor: '#847DFA',
                borderWidth: 0.5,
                pointStyle: 'circle',
                pointRadius: 5,
                pointBorderColor: 'transparent',
                pointBackgroundColor: '#847DFA',
            }]
        },
        options: {
            responsive: !0,
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                titleFontSize: 12,
                titleFontColor: '#000',
                bodyFontColor: '#000',
                backgroundColor: '#fff',
                titleFontFamily: 'Montserrat',
                bodyFontFamily: 'Montserrat',
                cornerRadius: 3,
                intersect: false,
            },
            legend: {
                display: false,
                position: 'top',
                labels: {
                    usePointStyle: true,
                    fontFamily: 'Montserrat',
                },


            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    scaleLabel: {
                        display: false,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: false,
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            },
            title: {
                display: false,
            }
        }
    });


    


})(jQuery);
</script>
@endsection