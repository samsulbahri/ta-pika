<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Bunga Bali Florist</title>
    <!-- Favicon icon -->
    <!-- Pignose Calender -->
    <link href="/dashboard/plugins/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <!-- Chartist -->
    <link rel="stylesheet" href="/dashboard/plugins/chartist/css/chartist.min.css">
    <link rel="stylesheet" href="/dashboard/plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css">
    <!-- Custom Stylesheet -->
    <link href="/dashboard/css/style.css" rel="stylesheet">

    @yield('css')
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3"
                    stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="/staff/home">
                    <a href="/manager/home">
                        <b class="text-white" style="font-size: 1.125rem;">Bunga Bali Florist</b>
                    </a>
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content clearfix">

                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <div class="header-right">
                    <ul class="clearfix">
                        <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative" data-toggle="dropdown">
                                <img src="/dashboard/images/user/1.png" height="40" width="40"
                                    alt="">
                            </div>
                            <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li><a href="/logout"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        
    <!--**********************************
                Sidebar start
            ***********************************-->
    <div class="nk-sidebar">
        <div class="nk-nav-scroll">
            <ul class="metismenu" id="menu">
                <li style="margin-top: 4px; margin-bottom: 4px;">
                    <a href="/staff/home" aria-expanded="false">
                        <i class="icon-speedometer menu-icon"></i><span class="nav-text">Beranda</span>
                    </a>
                </li>
                <li style="margin-top: 4px; margin-bottom: 4px;">
                    <a href="/staff/penjualan" aria-expanded="false">
                        <i class="icon-menu menu-icon"></i><span class="nav-text">Penjualan</span>
                    </a>
                </li>
                <li style="margin-top: 4px; margin-bottom: 4px;">
                    <a href="/staff/produk" aria-expanded="false">
                        <i class="icon-grid menu-icon"></i><span class="nav-text">Produk</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!--**********************************
                Sidebar end
            ***********************************-->

        @yield('main')


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a>
                    2018</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="/dashboard/plugins/common/common.min.js"></script>
    <script src="/dashboard/js/custom.min.js"></script>
    <script src="/dashboard/js/settings.js"></script>
    <script src="/dashboard/js/gleek.js"></script>
    <script src="/dashboard/js/styleSwitcher.js"></script>

    <!-- Chartjs -->
    <script src="/dashboard/plugins/chart.js/Chart.bundle.min.js"></script>
    <!-- Circle progress -->
    <script src="/dashboard/plugins/circle-progress/circle-progress.min.js"></script>
    <!-- Datamap -->
    <script src="/dashboard/plugins/d3v3/index.js"></script>
    <script src="/dashboard/plugins/topojson/topojson.min.js"></script>
    <script src="/dashboard/plugins/datamaps/datamaps.world.min.js"></script>
    <!-- Morrisjs -->
    <script src="/dashboard/plugins/raphael/raphael.min.js"></script>
    <script src="/dashboard/plugins/morris/morris.min.js"></script>
    <!-- Pignose Calender -->
    <script src="/dashboard/plugins/moment/moment.min.js"></script>
    <script src="/dashboard/plugins/pg-calendar/js/pignose.calendar.min.js"></script>
    <!-- ChartistJS -->
    <script src="/dashboard/plugins/chartist/js/chartist.min.js"></script>
    <script src="/dashboard/plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js"></script>



    <script src="/dashboard/js/dashboard/dashboard-1.js"></script>

    @yield('js')
</body>

</html>
