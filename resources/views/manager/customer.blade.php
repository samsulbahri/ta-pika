
@extends('base.root-manager')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap4.min.css">
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap4.min.js"></script>

<script>
    new DataTable('#table');
</script>
@endsection

@section('main')
    <div class="content-body">
        <div class="container-fluid mt-3">

            @if (session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session('error') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="active-member">
                                <div class="table-responsive">
                                    <table class="table table-xs mb-0" id="table">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Nomor Telepon</th>
                                                <th>Email</th>
                                                <th>Alamat</th>
                                                <th>Kota</th>
                                                <th>Provinsi</th>
                                                <th>Kode Pos</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($customers as $customer)
                                                <tr>
                                                    <td>{{ $customer->customer_name }}</td>
                                                    <td>{{ $customer->customer_phone_number }}</td>
                                                    <td>{{ $customer->customer_email }}</td>
                                                    <td>{{ $customer->customer_address }}</td>
                                                    <td>{{ $customer->customer_city }}</td>
                                                    <td>{{ $customer->customer_province }}</td>
                                                    <td>{{ $customer->customer_zip_code }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-warning" data-toggle="modal"
                                                            data-target="#editModal{{ $customer->customer_id }}">Edit</button>
                                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                            data-target="#deleteModal{{ $customer->customer_id }}">Delete</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @foreach ($customers as $customer)
        <div class="modal fade" id="editModal{{ $customer->customer_id }}" tabindex="-1" role="dialog"
            aria-labelledby="editModal{{ $customer->customer_id }}Label" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModal{{ $customer->customer_id }}Label">Ubah Data {{$customer->customer_name}}</h5> <button
                            type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="/manager/ubah-customer" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="customer_id" value="{{$customer->customer_id}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nama Customer</label>
                                <input required type="text" class="form-control" name="customer_name" value="{{$customer->customer_name}}">
                            </div>
                            <div class="form-group">
                                <label>Nomor Telpon Customer</label>
                                <input required type="text" class="form-control" name="customer_phone_number" value="{{$customer->customer_phone_number}}">
                            </div>
                            <div class="form-group">
                                <label>Email Customer</label>
                                <input required type="text" class="form-control" name="customer_email" value="{{$customer->customer_email}}">
                            </div>
                            <div class="form-group">
                                <label>Alamat Customer</label>
                                <input required type="text" class="form-control" name="customer_address" value="{{$customer->customer_address}}">
                            </div>
                            <div class="form-group">
                                <label>Kota Customer</label>
                                <input required type="text" class="form-control" name="customer_city" value="{{$customer->customer_city}}">
                            </div>
                            <div class="form-group">
                                <label>Provinsi Customer</label>
                                <input required type="text" class="form-control" name="customer_province" value="{{$customer->customer_province}}">
                            </div>
                            <div class="form-group">
                                <label>Kode Pos Customer</label>
                                <input required type="text" class="form-control" name="customer_zip_code" value="{{$customer->customer_zip_code}}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-warning">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="deleteModal{{ $customer->customer_id }}" tabindex="-1" role="dialog"
            aria-labelledby="deleteModal{{ $customer->customer_id }}Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModal{{ $customer->customer_id }}Label">Hapus Data {{$customer->customer_name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        anda yakin ingin menghapus Customer ini?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <a href="/manager/hapus-customer/{{$customer->customer_id}}" class="btn btn-danger">Ya, Hapus</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
