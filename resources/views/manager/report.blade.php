@php
    function rupiah($angka)
    {
        $hasil_rupiah = 'Rp' . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
@endphp
@extends('base.root-manager')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap4.min.css">
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap4.min.js"></script>

<script>
    new DataTable('#table');
    new DataTable('#table1');
</script>
@endsection

@section('main')
    <!--**********************************
                    Content body start
                ***********************************-->
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card gradient-2">
                        <div class="card-body">
                            <h3 class="card-title text-white">Penghasilan Harian</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white">{{ rupiah($totalDaily->quantity) }}</h2>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card gradient-1">
                        <div class="card-body">
                            <h3 class="card-title text-white">Penghasilan Bulanan</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white">{{ rupiah($totalMonthly->quantity) }}</h2>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt-3">

            @if (session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session('error') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-primary float-right mb-3" data-toggle="modal" data-target="#exportModal">Export
                        Data</button>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <p><h5>Rangkuman Pendapatan</h5></p>
                        </div>
                        <div class="card-body">
                            <div class="active-member">
                                <div class="table-responsive">
                                    <table class="table table-xs mb-0" id="table1">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Bulan</th>
                                                <th colspan="2" class="text-center">2023</th>
                                                <th colspan="2" class="text-center">2024</th>
                                            </tr>
                                            <tr>
                                                <th>Pendapatan</th>
                                                <th>Keuntungan</th>
                                                <th>Pendapatan</th>
                                                <th>Keuntungan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($sums as $sum)
                                            <tr>
                                                <td>{{$sum->bulan}}</td>
                                                <td>{{rupiah($sum->cap_prev)}}</td>
                                                <td>{{rupiah($sum->rev_prev)}}</td>
                                                <td>{{rupiah($sum->cap_next)}}</td>
                                                <td>{{rupiah($sum->rev_next)}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <p><h5>Detail Transaksi Bulan Ini</h5></p>
                        </div>
                        <div class="card-body">
                            <div class="active-member">
                                <div class="table-responsive">
                                    <table class="table table-xs mb-0" id="table">
                                        <thead>
                                            <tr>
                                                <th>Invoice</th>
                                                <th>Pembeli</th>
                                                <th>Barang</th>
                                                <th>Pembayaran</th>
                                                <th>Status Pembelian</th>
                                                <th>Link Pembayaran</th>
                                                <th>Tanggal Transaksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($detailOrders as $detailOrder)
                                            <tr>
                                                <td>#{{$detailOrder->invoice}}</td>
                                                <td>{{$detailOrder->customer_name}}</td>
                                                <td>{{$detailOrder->product_name}}</td>
                                                <td>{{$detailOrder->channel_name}}</td>
                                                @if ($detailOrder->status == 'ordered')
                                                <td><span class="badge badge-primary">Dipesan</span></td>
                                                @elseif ($detailOrder->status == 'fail_pay')
                                                <td><span class="badge badge-danger">Gagal Membayar</span></td>
                                                @elseif ($detailOrder->status == 'fail_shipping')
                                                <td><span class="badge badge-danger">Gagal Pengiriman</span></td>
                                                @elseif ($detailOrder->status == 'payed')
                                                <td><span class="badge badge-info">Dibayar</span></td>
                                                @elseif ($detailOrder->status == 'done')
                                                <td><span class="badge badge-success">Selesai</span></td>
                                                @endif
                                                <td><a target="__blank" href="{{$detailOrder->payment_url}}">{{$detailOrder->payment_url}}</a></td>
                                                <td>{{$detailOrder->order_date}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Export Data</h5> <button type="button" class="close"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="/manager/export-penjualan" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Bulan</label>
                            <select name="month" class="form-control">
                                <option value="all">Semua Bulan</option>
                                @foreach ($monthOrders as $monthOrder)
                                    @if ($monthOrder->month == date('m'))
                                        <option value="{{$monthOrder->month}}" selected>{{$monthOrder->month}}</option>
                                    @else
                                        <option value="{{$monthOrder->month}}">{{$monthOrder->month}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tahun</label>
                            <select name="year" class="form-control">
                                <option value="all">Semua Tahun</option>
                                @foreach ($yearOrders as $yearOrder)
                                    @if ($yearOrder->year == date('Y'))
                                        <option value="{{$yearOrder->year}}" selected>{{$yearOrder->year}}</option>
                                    @else
                                        <option value="{{$yearOrder->year}}">{{$yearOrder->year}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Download</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- #/ container -->
    </div>
    <!--**********************************
                    Content body end
                ***********************************-->
@endsection
