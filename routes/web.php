<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ManagerController;
use App\Http\Controllers\StaffController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Customer
Route::get('/', [CustomerController::class, 'home']);
Route::get('/tentang-kami', [CustomerController::class, 'aboutUs']);
Route::get('/belanja/{category}', [CustomerController::class, 'shop']);
Route::get('/detail-barang/{product}', [CustomerController::class, 'productDetail']);
Route::get('/cek-transaksi/{invoice}', [CustomerController::class, 'transactionDetail']);
Route::get('/keranjang', [CustomerController::class, 'cart']);
Route::get('/pesan', [CustomerController::class, 'order']);
Route::post('/tambahkan-ke-keranjang', [CustomerController::class, 'addToCart']);
Route::post('/update-cart', [CustomerController::class, 'updateCart']);
Route::post('/pesan', [CustomerController::class, 'order']);
Route::post('/rating', [CustomerController::class, 'rating']);

// Auth
Route::get('/login', [AuthController::class, 'login']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/register', [AuthController::class, 'register']);
Route::get('/logout', [AuthController::class, 'logout']);

// Manager
Route::get('/manager/home', [ManagerController::class, 'home']);
Route::get('/manager/produk', [ManagerController::class, 'product']);
Route::get('/manager/kategori', [ManagerController::class, 'category']);
Route::get('/manager/laporan', [ManagerController::class, 'report']);
Route::get('/manager/customer', [ManagerController::class, 'customer']);

Route::post('/manager/tambah-produk', [ManagerController::class, 'addProduct']);
Route::post('/manager/ubah-produk', [ManagerController::class, 'editProduct']);
Route::get('/manager/hapus-produk/{id}', [ManagerController::class, 'deleteProduct']);

Route::post('/manager/ubah-customer', [ManagerController::class, 'editCustomer']);
Route::get('/manager/hapus-customer/{id}', [ManagerController::class, 'deleteCustomer']);

Route::post('/manager/tambah-kategori', [ManagerController::class, 'addCategory']);
Route::post('/manager/ubah-kategori', [ManagerController::class, 'editCategory']);
Route::get('/manager/hapus-kategori/{id}', [ManagerController::class, 'deleteCategory']);

Route::post('/manager/export-penjualan', [ManagerController::class, 'exportOrder']);

// Staff
Route::get('/staff/home', [StaffController::class, 'home']);
Route::get('/staff/produk', [StaffController::class, 'product']);
Route::get('/staff/penjualan', [StaffController::class, 'order']);
Route::get('/staff/order-selesai/{id}', [StaffController::class, 'doneOrder']);

Route::post('/staff/ubah-stok', [StaffController::class, 'changeStock']);
Route::get('/staff/cek-pembayaran/{invoice}', [StaffController::class, 'checkPayment']);

