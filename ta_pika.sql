/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100427
 Source Host           : localhost:3306
 Source Schema         : ta_pika

 Target Server Type    : MySQL
 Target Server Version : 100427
 File Encoding         : 65001

 Date: 05/06/2024 01:38:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carts
-- ----------------------------
DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts`  (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `product_id` int NULL DEFAULT NULL,
  `customer_id` int NULL DEFAULT NULL,
  `order_id` int NULL DEFAULT NULL,
  `quantity` int NULL DEFAULT NULL,
  `rating_star` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `rating_message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cart_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of carts
-- ----------------------------
INSERT INTO `carts` VALUES (4, 1, 1, 6, 12, NULL, NULL);
INSERT INTO `carts` VALUES (5, 1, 1, 15, 7, '5', 'Keren');
INSERT INTO `carts` VALUES (7, 1, 1, 22, 5, '5', 'Jam tangannya ok');
INSERT INTO `carts` VALUES (8, 1, 1, 22, 4, '5', 'Kerenn');
INSERT INTO `carts` VALUES (9, 1, 1, 23, 1, NULL, NULL);
INSERT INTO `carts` VALUES (10, 1, 2, 24, 2, NULL, NULL);
INSERT INTO `carts` VALUES (11, 1, 1, NULL, 1, NULL, NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Baju');
INSERT INTO `categories` VALUES (2, 'Jam Tangan');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `customer_phone_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `customer_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `customer_address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `customer_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `customer_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `customer_zip_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`customer_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 5, 'Samsul Bahri', '6281277057373', 'samsulbahrim25@gmail.com', 'lashdajsk', 'ladjkals', 'a;odka;', 'lakkdjao');
INSERT INTO `customers` VALUES (2, 7, 'Felix', '081277057373', 'felix@gmail.com', 'JL Flamboyan RT 23 Kelurahan Baru Ulu Kecamatan Balikpapan Barat', 'Balikpapan', 'Kalimantan Timur', '76133');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NULL DEFAULT NULL,
  `channel_id` int NULL DEFAULT NULL,
  `invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `total_price` int NULL DEFAULT NULL,
  `status` enum('ordered','fail_pay','fail_shipping','payed','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `payment_invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `payment_expired` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `payment_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `order_date` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (6, 1, 11, 'BBF0000000001', 10000, 'done', 'DEV-T25736126905P4DYN', '1697965493', 'https://tripay.co.id/checkout/DEV-T25736126905P4DYN', NULL, '2023-10-27 15:47:56');
INSERT INTO `orders` VALUES (15, 1, 11, 'BBF0000000002', 35000, 'done', 'DEV-T25736126925IQPMQ', '1697975225', 'https://tripay.co.id/checkout/DEV-T25736126925IQPMQ', NULL, '2023-11-17 22:07:04');
INSERT INTO `orders` VALUES (22, 1, 11, 'BBF0000000003', 25000, 'done', 'DEV-T25736129428YAYIX', '1699711056', 'https://tripay.co.id/checkout/DEV-T25736129428YAYIX', 'tolong dikemas dengan baik', '2023-11-18 19:44:53');
INSERT INTO `orders` VALUES (23, 1, 11, 'BBF0000000004', 5000, 'ordered', 'DEV-T25736154401X60ZQ', '1715011711', 'https://tripay.co.id/checkout/DEV-T25736154401X60ZQ', NULL, '2024-05-06 23:09:31');
INSERT INTO `orders` VALUES (24, 2, 11, 'BBF0000000005', 10000, 'ordered', 'DEV-T25736156047UEVWS', '1716184942', 'https://tripay.co.id/checkout/DEV-T25736156047UEVWS', 'Mohon untuk dikirim besok pagi', '2024-05-20 13:03:22');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `category_id` int NULL DEFAULT NULL,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `product_capital` int NULL DEFAULT NULL,
  `product_price` int NULL DEFAULT NULL,
  `product_image_1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `product_image_2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `product_image_3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `product_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `product_quantity` int NULL DEFAULT NULL,
  `product_date_added` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 1, 'Jam Tangan Keren', NULL, 5000, '/image/1699447956_jam_tangan_keren_1.jpg', '/image/jamtangan2.png', '/image/jamtangan3.png', 'Lorem Ipsum', 94, '2024-06-05 01:08:55');

-- ----------------------------
-- Table structure for summary_transactions
-- ----------------------------
DROP TABLE IF EXISTS `summary_transactions`;
CREATE TABLE `summary_transactions`  (
  `summary_transaction_id` int NOT NULL AUTO_INCREMENT,
  `summary_transaction_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `summary_transaction_capital` int NULL DEFAULT NULL,
  `summary_transaction_revenue` int NULL DEFAULT NULL,
  `summary_transaction_lots` int NULL DEFAULT NULL,
  `summary_transaction_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`summary_transaction_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of summary_transactions
-- ----------------------------
INSERT INTO `summary_transactions` VALUES (1, 'Jam Tangan Keren', 4000, 1000, 2, '2024-06-05');

-- ----------------------------
-- Table structure for tripay_channels
-- ----------------------------
DROP TABLE IF EXISTS `tripay_channels`;
CREATE TABLE `tripay_channels`  (
  `channel_id` int NOT NULL AUTO_INCREMENT,
  `channel_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `channel_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `channel_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `channel_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fee_merchant_flat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fee_merchant_percent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fee_customer_flat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fee_customer_percent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `total_fee_flat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `total_fee_percent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `minimum_fee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `maximum_fee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `channel_icon_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `channel_active` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`channel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tripay_channels
-- ----------------------------
INSERT INTO `tripay_channels` VALUES (1, 'MYBVA', 'Maybank Virtual Account', 'Virtual Account', 'direct', '0', '0', '4250', '0', '4250', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/ZT91lrOEad1582929126.png', '1');
INSERT INTO `tripay_channels` VALUES (2, 'PERMATAVA', 'Permata Virtual Account', 'Virtual Account', 'direct', '0', '0', '4250', '0', '4250', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/szezRhAALB1583408731.png', '1');
INSERT INTO `tripay_channels` VALUES (3, 'BRIVA', 'BRI Virtual Account', 'Virtual Account', 'direct', '0', '0', '4250', '0', '4250', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/8WQ3APST5s1579461828.png', '1');
INSERT INTO `tripay_channels` VALUES (4, 'MANDIRIVA', 'Mandiri Virtual Account', 'Virtual Account', 'direct', '0', '0', '4250', '0', '4250', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/T9Z012UE331583531536.png', '1');
INSERT INTO `tripay_channels` VALUES (5, 'BCAVA', 'BCA Virtual Account', 'Virtual Account', 'direct', '0', '0', '5500', '0', '5500', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/ytBKvaleGy1605201833.png', '1');
INSERT INTO `tripay_channels` VALUES (6, 'BSIVA', 'BSI Virtual Account', 'Virtual Account', 'direct', '0', '0', '4250', '0', '4250', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/tEclz5Assb1643375216.png', '1');
INSERT INTO `tripay_channels` VALUES (7, 'DANAMONVA', 'Danamon Virtual Account', 'Virtual Account', 'direct', '0', '0', '4250', '0', '4250', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/F3pGzDOLUz1644245546.png', '1');
INSERT INTO `tripay_channels` VALUES (8, 'ALFAMART', 'Alfamart', 'Convenience Store', 'direct', '0', '0', '3500', '0', '3500', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/jiGZMKp2RD1583433506.png', '1');
INSERT INTO `tripay_channels` VALUES (9, 'INDOMARET', 'Indomaret', 'Convenience Store', 'direct', '0', '0', '3500', '0', '3500', '0.00', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/zNzuO5AuLw1583513974.png', '1');
INSERT INTO `tripay_channels` VALUES (10, 'OVO', 'OVO', 'E-Wallet', 'redirect', '0', '0', '0', '3', '0', '3.00', '1000', NULL, 'https://assets.tripay.co.id/upload/payment-icon/fH6Y7wDT171586199243.png', '1');
INSERT INTO `tripay_channels` VALUES (11, 'QRIS2', 'QRIS', 'E-Wallet', 'direct', '0', '0', '750', '0.7', '750', '0.70', NULL, NULL, 'https://assets.tripay.co.id/upload/payment-icon/8ewGzP6SWe1649667701.png', '1');
INSERT INTO `tripay_channels` VALUES (12, 'DANA', 'DANA', 'E-Wallet', 'redirect', '0', '0', '0', '3', '0', '3.00', '1000', NULL, 'https://assets.tripay.co.id/upload/payment-icon/sj3UHLu8Tu1655719621.png', '1');
INSERT INTO `tripay_channels` VALUES (13, 'SHOPEEPAY', 'ShopeePay', 'E-Wallet', 'redirect', '0', '0', '0', '3', '0', '3.00', '1000', NULL, 'https://assets.tripay.co.id/upload/payment-icon/d204uajhlS1655719774.png', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `level` enum('manager','staff','customer') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'samsulbahri', '$2a$12$SvSHnsgqplO7SyV/JZOK1Oe7OWJJvLfoYkKm/arFSNsTKg8srFyvK', 'manager');
INSERT INTO `users` VALUES (5, 'anureta', '$2y$10$f4fU2f9INDGEPD8OyamsXuFS8dxNghUYZRZmSOXqhx/Knpzk4z9Du', 'customer');
INSERT INTO `users` VALUES (6, 'kroco', '$2y$10$f4fU2f9INDGEPD8OyamsXuFS8dxNghUYZRZmSOXqhx/Knpzk4z9Du', 'staff');
INSERT INTO `users` VALUES (7, 'felix', '$2y$10$sGTJr6aR5oL98WoTmcO8uel6xxjMMmPV3QuO5UWC0n1dyi7rD0VO2', 'customer');

SET FOREIGN_KEY_CHECKS = 1;
