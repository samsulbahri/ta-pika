<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    public $timestamps = false;
    protected $primaryKey = 'order_id';
    protected $keyType = 'string';

    protected $fillable = [
        'order_id',
        'customer_id',
        'channel_id',
        'invoice',
        'total_price',
        'status',
        'payment_invoice',
        'payment_expired',
        'payment_url',
        'message',
        'order_date',
    ];

    public function tripay_channel(): BelongsTo
    {
        return $this->belongsTo(TripayChannel::class, 'channel_id');
    }
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
