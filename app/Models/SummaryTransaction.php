<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SummaryTransaction extends Model
{
    use HasFactory;
    protected $table = 'summary_transactions';
    public $timestamps = false;
    protected $primaryKey = 'summary_transaction_id';
    protected $keyType = 'string';

    protected $fillable = [
        'summary_transaction_name',
        'summary_transaction_capital',
        'summary_transaction_revenue',
        'summary_transaction_lots',
        'summary_transaction_date',
    ];
}
