<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    public $timestamps = false;
    protected $primaryKey = 'product_id';
    protected $keyType = 'string';
    protected $fillable = [
        'product_id',
        'product_name',
        'product_capital',
        'product_price',
        'product_image_1',
        'product_image_2',
        'product_image_3',
        'product_description',
        'product_quantity',
        'category_id',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function cart(): HasOne {
        return $this->hasOne(Cart::class, 'product_id');
    }
}
