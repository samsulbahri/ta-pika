<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;
    protected $table = 'categories';
    public $timestamps = false;
    protected $primaryKey = 'category_id';
    protected $keyType = 'string';
    protected $fillable = [
        'category_id',
        'category_name',
    ];

    public function product(): HasMany {
        return $this->hasMany(Product::class, 'category_id');
    }
}
