<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\TripayChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CustomerController extends Controller
{
    protected $apiController;
    public function __construct(APIController $apiController)
    {
        $this->apiController = $apiController;
    }

    public function home()
    {
        $categories = Category::all();
        $newArrivals = DB::select("SELECT * FROM products ORDER BY product_date_added DESC LIMIT 3");
        $hotProducts = DB::select("SELECT count(a.product_id) as pembelian, b.product_name, b.product_price, b.product_image_1 from carts a inner join products b on a.product_id = b.product_id GROUP BY 2,3,4");
        return view('customer/landing', ['categories' => $categories, 'newArrivals' => $newArrivals, 'hotProducts' => $hotProducts]);
    }

    public function aboutUs()
    {
        $categories = Category::all();
        return view('customer/about-us', ['categories' => $categories]);
    }

    public function shop($category) {
        $category = str_replace("_", " ", $category);
        $categorySelected = DB::select("SELECT * FROM categories WHERE LOWER(category_name) = '" . $category . "'");
        if($categorySelected == null) {
            abort(404);
        }

        $categorySelected = $categorySelected[0];
        $categories = Category::all();
        $products = Product::where('category_id', $categorySelected->category_id)->get();
        return view('customer/category-detail', ['categories' => $categories, 'categoryName' => $categorySelected->category_name, 'products' => $products]);
    }

    public function productDetail($product) {
        $categories = Category::all();
        $product = strtoupper(str_replace("_", " ", $product));
        $dbProduct = DB::select("SELECT * FROM products WHERE upper(product_name) = '$product';");
        $reviews = Cart::with('product')->where('product_id', $dbProduct[0]->product_id)->where('rating_star', '!=', null)->get();
        if($dbProduct) {
            return view('customer/product-detail', ['categories' => $categories, 'reviews' => $reviews, 'dbProduct' => $dbProduct[0]]);
        } else {
            abort(404);
        }
    }

    public function transactionDetail($invoice) {
        if(Auth::guest()) {
            return redirect("/login")->with('error', 'Silakan login terlebih dahulu sebelum mulai bertransaksi');
        }
        $categories = Category::all();
        $order = Order::with(['tripay_channel', 'customer'])->where('invoice', $invoice)->first();
        $detailOrders = Cart::with('product')->where('order_id', $order->order_id)->get();
        if($order) {
            return view('customer/transaction-detail', ['categories' => $categories, 'order' => $order, 'detailOrders' => $detailOrders]);
        } else {
            abort(404);
        }
    }

    public function addToCart(Request $request) {
        if(Auth::guest()) {
            return redirect("/login")->with('error', 'Silakan login terlebih dahulu sebelum mulai bertransaksi');
        }

        $customerId = Customer::where('user_id', Auth::id())->first();

        $cekCart = Cart::where(['product_id' => $request->product, 'customer_id' => $customerId->customer_id, 'order_id' => null])->first();
        if($cekCart) {
            $cekCart->quantity = $cekCart->quantity + $request->quantity;
            $cekCart->save();
        } else {
            $cart = new Cart();
            $cart->product_id = $request->product;
            $cart->customer_id = $customerId->customer_id;
            $cart->quantity = $request->quantity;
            $cart->save();
        }
        
        return back()->with('success', 'success');
    }

    public function updateCart(Request $request) {
        if(Auth::guest()) {
            return redirect("/login")->with('error', 'Silakan login terlebih dahulu sebelum mulai bertransaksi');
        }
        foreach ($request->except('_token') as $key => $part) {
            $cart = Cart::where('cart_id', explode("_", $key)[1])->first();
            $cart->quantity = $part;
            if($part == "0") {
                $cart->delete();
            } else {
                $cart->save();
            }
          }
          return back()->with('success', 'success');
    }

    public function cart() {
        if(Auth::guest()) {
            return redirect("/login")->with('error', 'Silakan login terlebih dahulu sebelum mulai bertransaksi');
        }
        $categories = Category::all();
        $customerId = Customer::where('user_id', Auth::id())->first();
        $carts = Cart::with('product')->where('customer_id', $customerId->customer_id)->where('order_id', null)->get();
        return view('customer/cart', ['categories' => $categories, 'carts' => $carts]);
    }

    public function order(Request $request) {
        if(Auth::guest()) {
            return redirect("/login")->with('error', 'Silakan login terlebih dahulu sebelum mulai bertransaksi');
        }
        $customer = Customer::where('user_id', Auth::id())->first();
        $carts = Cart::with('product')->where('customer_id', $customer->customer_id)->where('order_id', null)->get();
        if($request->method() == 'GET') {
            $categories = Category::all();
            $paymentGroups = TripayChannel::distinct()->get(['channel_group']);
            $paymentChannels = TripayChannel::all();
            return view('customer/order', ['categories' => $categories, 'carts' => $carts, 'customer' => $customer, 'paymentGroups' => $paymentGroups, 'paymentChannels' => $paymentChannels]);
        } else if($request->method() == 'POST') {
            $customer = Customer::where('user_id', Auth::id())->first();
            $customer->customer_name = $request->customer_name;
            $customer->customer_phone_number = $request->customer_phone_number;
            $customer->customer_email = $request->customer_email;
            $customer->customer_address = $request->customer_address;
            $customer->customer_city = $request->customer_city;
            $customer->customer_province = $request->customer_province;
            $customer->customer_zip_code = $request->customer_zip_code;
            $customer->save();

            $lastTransaction = Order::orderBy('invoice', 'desc')->first();
            $index = 0;
            if ($lastTransaction != null) {
                $index = (int)substr($lastTransaction->invoice, 5);
            }

            $invoice = 'BBF' . sprintf('%0' . 10 . 's', $index + 1);
            $totalPrice = 0;
            foreach ($carts as $cart) {
                $totalPrice += $cart->quantity * $cart->product->product_price;
            }

            $order = new Order();
            $order->customer_id = $customer->customer_id;
            $order->channel_id = $request->channel_id;
            $order->invoice = $invoice;
            $order->total_price = $totalPrice;
            $order->status = 'ordered';
            $order->message = $request->message;
            
            $order->save();
            foreach ($carts as $cart) {
                $cart->order_id = $order->order_id;

                $product = Product::find($cart->product_id);
                $product->product_quantity = $product->product_quantity - $cart->quantity;

                if($product->product_quantity < $cart->quantity) {
                    foreach($carts as $cartDelete) {
                        $cartDelete->order_id = null;
                        $cartDelete->save();
                    }

                    $order->delete();

                    abort(404);
                }
                $product->save();
                $cart->save();


            }
            
            $makePayment = $this->apiController->createTransactionTripay($order);

            if ($makePayment->success) {
                $data = $makePayment->data;
                $order->payment_invoice = $data->reference;
                $order->payment_expired = $data->expired_time;
                $order->payment_url = $data->checkout_url;
                $order->save();

                return Redirect($data->checkout_url);
            } else {
                foreach ($carts as $cart) {
                    $cart->order_id = null;
                    $cart->save();
                }
                $order->delete();
                return $makePayment;
                return redirect()->back()->with('failed', 'Gagal Melakukan transaksi, silahkan hubungi admin kami');
            }
        }
    }

    public function rating(Request $request) {
        if(Auth::guest()) {
            return redirect("/login")->with('error', 'Silakan login terlebih dahulu sebelum mulai bertransaksi');
        }

        $cart = Cart::find($request->id);
        $cart->rating_star = $request->rating;
        $cart->rating_message = $request->message;
        $cart->save();
        return back()->with('success', 'Terimakasih atas reviewnya Kak!');
    }
}
