<?php

namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::guest()) {
                if (Auth::user()->level == 'manager') {
                    Redirect::to("/manager/home")->send();
                }
                if (Auth::user()->level == 'customer') {
                    Redirect::to("/")->send();
                }
            }

            return $next($request);
        });
        
    }

    public function login(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth/login');
        } else if ($request->method() == 'POST') {
            $user = User::where('username', $request->username)->first();
            if(Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
                if($user->level == 'manager') {
                    Auth::login($user);
                    return redirect('/manager/home');
                }elseif($user->level == 'customer') { 
                    Auth::login($user);
                    return redirect('/');
                }else {
                    return back()->with('error', 'Akun anda tidak dapat diidentifikasi, silahkan hubungi Admin!');
                }
            } else {
                return back()->with('error', 'Username atau Password salah!');
            }
        }
    }

    public function register(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth/register');
        } else if ($request->method() == 'POST') {
            $user = User::where('username', $request->username)->first();
            if($user != null) {
                return back()->with('error', 'Username anda telah terdaftar! Silahkan mendaftar menggunakan username lain.');
            }

            $newUser = new User();
            $newUser->username = $request->username;
            $newUser->password = Hash::make($request->password);
            $newUser->level = 'customer';
            $newUser->save();

            $customer = new Customer();
            $customer->user_id = $newUser->user_id;
            $customer->customer_name = $request->name;
            $customer->customer_phone_number = $request->phone_number;
            $customer->customer_email = $request->email;
            $customer->save();

            Auth::login($newUser);
            return redirect('/');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/login');
    }
}
