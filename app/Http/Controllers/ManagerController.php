<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ManagerController extends Controller
{
    private $maxSize = 10048576;
    protected $apiController;
    public function __construct(APIController $apiController)
    {
        $this->apiController = $apiController;
        $this->middleware(function ($request, $next) {
            if (Auth::guest()) {
                Redirect::to("/login")->send();
            }

            if (Auth::user()->level != 'manager') {
                Redirect::to("/logout")->send();
            }

            return $next($request);
        });
    }

    public function home()
    {
        $totalOrder = DB::select("SELECT  case when SUM(b.quantity) is null then 0 else sum(b.quantity) end as quantity FROM `orders` a inner join carts b on a.order_id = b.order_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y'))[0];
        $totalRev = DB::select("SELECT SUM(b.quantity * c.product_price) as quantity FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y'))[0];
        $totalCustomer = Customer::count();
        $totalReview = Cart::where('rating_star', '!=', null)->count();
        $query = '';
        for ($date = 1; $date <= date('t'); $date++) {
            if ($date == date('t')) {
                $query .= "SELECT " . $date . " as day, case when sum(c.product_price) is null then 0 else sum(c.product_price * b.quantity) end as income FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y') . " and day(a.order_date) = " . $date . ";";
            } else {
                $query .= "SELECT " . $date . " as day, case when sum(c.product_price) is null then 0 else sum(c.product_price * b.quantity) end as income FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y') . " and day(a.order_date) = " . $date . "
            
                UNION ALL
                
                ";
            }
        }

        $dailyIncome = DB::select($query);
        $dataSaleCustomers = Order::with(['customer', 'tripay_channel'])->orderBy('order_date', 'desc')->take(10)->get();
        return view('manager/home', ['totalOrder' => $totalOrder, 'totalRev' => $totalRev, 'totalCustomer' => $totalCustomer, 'totalReview' => $totalReview, 'dailyIncome' => $dailyIncome, 'dataSaleCustomers' => $dataSaleCustomers]);
    }

    public function category(Request $request)
    {
        if ($request->method() == 'GET') {
            $categories = Category::all();
            return view('manager/category', ['categories' => $categories]);
        }
    }

    public function addCategory(Request $request)
    {
        $category = new Category();
        $category->category_name = $request->category_name;
        $category->save();
        return redirect()->back()->with('success', 'Berhasil menambah kategori');
    }

    public function editCategory(Request $request)
    {
        $category = Category::find($request->category_id);
        $category->category_name = $request->category_name;
        $category->save();
        return redirect()->back()->with('success', 'Berhasil mengubah kategori');
    }

    public function deleteCategory($id)
    {
        $catergory = Category::find($id);
        $catergory->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus kategori ' . $catergory->category_name);
    }
    

    public function customer()
    {
        $customers = Customer::all();
        $categories = Category::all();
        return view('manager/customer', ['customers' => $customers, 'categories' => $categories]);
    }

    
    public function editCustomer(Request $request)
    {
        $customer = Customer::find($request->customer_id);
        $customer->customer_name = $request->customer_name;
        $customer->customer_phone_number = $request->customer_phone_number;
        $customer->customer_email = $request->customer_email;
        $customer->customer_address = $request->customer_address;
        $customer->customer_city = $request->customer_city;
        $customer->customer_province = $request->customer_province;
        $customer->customer_zip_code = $request->customer_zip_code;


        $customer->save();
        return redirect()->back()->with('success', 'Berhasil mengubah data' . $customer->customer_name);
    }

    public function deleteCustomer($id)
    {

        $customer = Customer::find($id);
        $access = User::find($customer->user_id);

        $access->delete();
        $customer->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data ' . $customer->customer_name);
    }

    public function product()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('manager/product', ['products' => $products, 'categories' => $categories]);
    }

    public function addProduct(Request $request)
    {
        $product = new Product();
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_capital = $request->product_capital;
        $product->product_price = $request->product_price;
        $product->product_description = $request->product_description;
        $product->product_quantity = $request->product_quantity;

        $toPhoto = '/image';
        $image1 = $request->file('product_image_1');
        $namePhoto1 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_1.jpg";

        if ($image1->getSize() > $this->maxSize) {
            return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
        }

        $image1->move(public_path($toPhoto), $namePhoto1);

        $image2 = $request->file('product_image_2');
        $namePhoto2 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_2.jpg";

        if ($image2->getSize() > $this->maxSize) {
            return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
        }

        $image2->move(public_path($toPhoto), $namePhoto2);

        $image3 = $request->file('product_image_3');
        $namePhoto3 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_3.jpg";

        if ($image3->getSize() > $this->maxSize) {
            return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
        }

        $image3->move(public_path($toPhoto), $namePhoto3);

        $product->product_image_1 = $toPhoto . '/' . $namePhoto1;
        $product->product_image_2 = $toPhoto . '/' . $namePhoto2;
        $product->product_image_3 = $toPhoto . '/' . $namePhoto3;

        $product->save();
        return redirect()->back()->with('success', 'Berhasil menambah produk');
    }

    public function editProduct(Request $request)
    {
        $product = Product::find($request->product_id);
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_capital = $request->product_capital;
        $product->product_price = $request->product_price;
        $product->product_description = $request->product_description;
        $product->product_quantity = $request->product_quantity;


        $toPhoto = '/image';
        if ($request->has('product_image_1')) {
            File::delete(public_path($product->product_image_1));
            $image1 = $request->file('product_image_1');
            $namePhoto1 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_1.jpg";

            if ($image1->getSize() > $this->maxSize) {
                return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
            }

            $image1->move(public_path($toPhoto), $namePhoto1);

            $product->product_image_1 = $toPhoto . '/' . $namePhoto1;
        }

        if ($request->has('product_image_2')) {
            File::delete(public_path($product->product_image_2));
            $image2 = $request->file('product_image_2');
            $namePhoto2 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_2.jpg";

            if ($image2->getSize() > $this->maxSize) {
                return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
            }

            $image2->move(public_path($toPhoto), $namePhoto2);

            $product->product_image_2 = $toPhoto . '/' . $namePhoto2;
        }

        if ($request->has('product_image_3')) {
            File::delete(public_path($product->product_image_3));
            $image3 = $request->file('product_image_3');
            $namePhoto3 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_3.jpg";

            if ($image3->getSize() > $this->maxSize) {
                return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
            }

            $image3->move(public_path($toPhoto), $namePhoto3);

            $product->product_image_3 = $toPhoto . '/' . $namePhoto3;
        }

        $product->save();
        return redirect()->back()->with('success', 'Berhasil mengubah produk' . $product->product_name);
    }

    public function deleteProduct($id)
    {

        $product = Product::find($id);
        File::delete(public_path($product->product_image_1));
        File::delete(public_path($product->product_image_2));
        File::delete(public_path($product->product_image_3));
        $product->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus produk ' . $product->product_name);
    }

    public function report()
    {
        $totalDaily = DB::select("SELECT SUM(b.quantity * c.product_price) as quantity FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y') . " and day(a.order_date) = " . date('d'))[0];
        $totalMonthly = DB::select("SELECT SUM(b.quantity * c.product_price) as quantity FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y'))[0];
        $detailOrders = DB::select("SELECT * FROM orders a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id inner join customers d on b.customer_id = d.customer_id inner join tripay_channels e on a.channel_id = e.channel_id where month(a.order_date) = " . date('m'));
        $monthOrders = DB::select("select month(order_date) as month from orders GROUP BY 1 order by 1");
        $yearOrders = DB::select("select year(order_date) as year from orders GROUP BY 1 order by 1");

        // Query Tahun ini Tahun Sebelumnya
        $yearNow = (int) date('Y');
        $yearBefore = $yearNow - 1;
        $query = '';
        $months = [
            1 => "Januari",
            2 => "Februari",
            3 => "Marret",
            4 => "April",
            5 => "Mei",
            6 => "Juni",
            7 => "Juli",
            8 => "Agustus",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Desember",
        ];

        foreach($months as $key => $value) {
            if($key != 12) {
                $query .= '
                SELECT 
                    "'.$value.'" as bulan, 
                    (SELECT 
                        CASE WHEN SUM(summary_transaction_capital + summary_transaction_revenue) IS NULL 
                        THEN 0 ELSE SUM(summary_transaction_capital + summary_transaction_revenue) END
                    FROM summary_transactions 
                    WHERE 
                        MONTH(summary_transaction_date) = '.$key.' AND 
                        YEAR(summary_transaction_date) = '.$yearBefore.') as cap_prev,
                    (SELECT 
                        CASE WHEN SUM(summary_transaction_revenue) IS NULL 
                        THEN 0 ELSE SUM(summary_transaction_revenue) END
                    FROM summary_transactions 
                    WHERE 
                        MONTH(summary_transaction_date) = '.$key.' AND 
                        YEAR(summary_transaction_date) = '.$yearBefore.') as rev_prev,
                    (SELECT 
                        CASE WHEN SUM(summary_transaction_capital + summary_transaction_revenue) IS NULL 
                        THEN 0 ELSE SUM(summary_transaction_capital + summary_transaction_revenue) END
                    FROM summary_transactions 
                    WHERE 
                        MONTH(summary_transaction_date) = '.$key.' AND 
                        YEAR(summary_transaction_date) = '.$yearNow.') as cap_next,
                    (SELECT 
                        CASE WHEN SUM(summary_transaction_revenue) IS NULL 
                        THEN 0 ELSE SUM(summary_transaction_revenue) END
                    FROM summary_transactions 
                    WHERE 
                        MONTH(summary_transaction_date) = '.$key.' AND 
                        YEAR(summary_transaction_date) = '.$yearNow.') as rev_next
                FROM summary_transactions

                UNION ALL
                ';
            } else {
            $query .= '
            SELECT 
                "'.$value.'" as bulan, 
                (SELECT 
                    CASE WHEN SUM(summary_transaction_capital + summary_transaction_revenue) IS NULL 
                    THEN 0 ELSE SUM(summary_transaction_capital + summary_transaction_revenue) END
                FROM summary_transactions 
                WHERE 
                    MONTH(summary_transaction_date) = '.$key.' AND 
                    YEAR(summary_transaction_date) = '.$yearBefore.') as cap_prev,
                (SELECT 
                    CASE WHEN SUM(summary_transaction_revenue) IS NULL 
                    THEN 0 ELSE SUM(summary_transaction_revenue) END
                FROM summary_transactions 
                WHERE 
                    MONTH(summary_transaction_date) = '.$key.' AND 
                    YEAR(summary_transaction_date) = '.$yearBefore.') as rev_prev,
                (SELECT 
                    CASE WHEN SUM(summary_transaction_capital + summary_transaction_revenue) IS NULL 
                    THEN 0 ELSE SUM(summary_transaction_capital + summary_transaction_revenue) END
                FROM summary_transactions 
                WHERE 
                    MONTH(summary_transaction_date) = '.$key.' AND 
                    YEAR(summary_transaction_date) = '.$yearNow.') as cap_next,
                (SELECT 
                    CASE WHEN SUM(summary_transaction_revenue) IS NULL 
                    THEN 0 ELSE SUM(summary_transaction_revenue) END
                FROM summary_transactions 
                WHERE 
                    MONTH(summary_transaction_date) = '.$key.' AND 
                    YEAR(summary_transaction_date) = '.$yearNow.') as rev_next
            FROM summary_transactions
            ';
            }
        }

        $sums = DB::select($query);
        return view('manager/report', ['totalDaily' => $totalDaily, 'totalMonthly' => $totalMonthly, 'detailOrders' => $detailOrders, 'monthOrders' => $monthOrders, 'yearOrders' => $yearOrders, 'sums' => $sums]);
    }

    public function exportOrder(Request $request)
    {
        $query = "SELECT a.invoice, a.total_price, a.status, a.payment_invoice, a.order_date, b.quantity as buy_quantity, c.product_name, c.product_price, d.customer_name, d.customer_phone_number, e.channel_name FROM orders a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id inner join customers d on b.customer_id = d.customer_id inner join tripay_channels e on a.channel_id = e.channel_id";

        if ($request->month != "all") {
            $query .= " where month(a.order_date) = " . $request->month;
        }

        if ($request->year != "all") {
            if ($request->month == "all") {
                $query .= " where year(a.order_date) = " . $request->year;
            } else {
                $query .= " and year(a.order_date) = " . $request->year;
            }
        }
        $datas = DB::select($query);
        $nameCsv = "data_pembelian_" . uniqid() . ".csv";

        $csvHeader = [
            'invoice',
            'total_price',
            'status',
            'payment_invoice',
            'order_date',
            'buy_quantity',
            'product_name',
            'product_price',
            'customer_name',
            'customer_phone_number',
            'channel_name',
        ];

        $callback = function () use ($datas, $csvHeader) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $csvHeader);


            foreach ($datas as $data) {
                fputcsv($file, array($data->invoice, $data->total_price, $data->status, $data->payment_invoice, $data->order_date, $data->buy_quantity, $data->product_name, $data->product_price, $data->customer_name, $data->customer_phone_number, $data->channel_name));
            }

            fclose($file);
        };

        $headersPHP = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=" . $nameCsv,
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0",
        );

        return response()->stream($callback, 200, $headersPHP);
    }
}
