<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Customer;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class StaffController extends Controller
{
    private $maxSize = 10048576;
    protected $apiController;
    public function __construct(APIController $apiController)
    {
        $this->apiController = $apiController;
        $this->middleware(function ($request, $next) {
            if (Auth::guest()) {
                Redirect::to("/login")->send();
            }
            return $next($request);
        });
    }
    public function home()
    {
        $totalOrder = DB::select("SELECT  case when SUM(b.quantity) is null then 0 else sum(b.quantity) end as quantity FROM `orders` a inner join carts b on a.order_id = b.order_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y'))[0];
        $totalRev = DB::select("SELECT SUM(b.quantity * c.product_price) as quantity FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y'))[0];
        $totalCustomer = Customer::count();
        $totalReview = Cart::where('rating_star', '!=', null)->count();
        $query = '';
        for ($date = 1; $date <= date('t'); $date++) {
            if ($date == date('t')) {
                $query .= "SELECT " . $date . " as day, case when sum(c.product_price) is null then 0 else sum(c.product_price * b.quantity) end as income FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y') . " and day(a.order_date) = " . $date . ";";
            } else {
                $query .= "SELECT " . $date . " as day, case when sum(c.product_price) is null then 0 else sum(c.product_price * b.quantity) end as income FROM `orders` a inner join carts b on a.order_id = b.order_id inner join products c on b.product_id = c.product_id WHERE `status` = 'done' and month(a.order_date) = " . date('m') . " and year(a.order_date) = " . date('Y') . " and day(a.order_date) = " . $date . "
            
                UNION ALL
                
                ";
            }
        }

        $dailyIncome = DB::select($query);
        $dataSaleCustomers = Order::with(['customer', 'tripay_channel'])->orderBy('order_date', 'desc')->take(10)->get();
        return view('staff/home', ['totalOrder' => $totalOrder, 'totalRev' => $totalRev, 'totalCustomer' => $totalCustomer, 'totalReview' => $totalReview, 'dailyIncome' => $dailyIncome, 'dataSaleCustomers' => $dataSaleCustomers]);
    }

    public function product()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('staff/product', ['products' => $products, 'categories' => $categories]);
    }

    public function addProduct(Request $request)
    {
        $product = new Product();
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_price = $request->product_price;
        $product->product_description = $request->product_description;
        $product->product_quantity = $request->product_quantity;

        $toPhoto = '/image';
        $image1 = $request->file('product_image_1');
        $namePhoto1 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_1.jpg";

        if ($image1->getSize() > $this->maxSize) {
            return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
        }

        $image1->move(public_path($toPhoto), $namePhoto1);

        $image2 = $request->file('product_image_2');
        $namePhoto2 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_2.jpg";

        if ($image2->getSize() > $this->maxSize) {
            return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
        }

        $image2->move(public_path($toPhoto), $namePhoto2);

        $image3 = $request->file('product_image_3');
        $namePhoto3 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_3.jpg";

        if ($image3->getSize() > $this->maxSize) {
            return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
        }

        $image3->move(public_path($toPhoto), $namePhoto3);

        $product->product_image_1 = $toPhoto . '/' . $namePhoto1;
        $product->product_image_2 = $toPhoto . '/' . $namePhoto2;
        $product->product_image_3 = $toPhoto . '/' . $namePhoto3;

        $product->save();
        return redirect()->back()->with('success', 'Berhasil menambah produk');
    }

    public function doneOrder($id) {
        $model = Order::find($id);
        $model->status = 'done';
        $model->save();
        
        return redirect()->back()->with('success', 'Berhasil menyelesaikan pesanan');
    }

    public function editProduct(Request $request)
    {
        $product = Product::find($request->product_id);
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_price = $request->product_price;
        $product->product_description = $request->product_description;
        $product->product_quantity = $request->product_quantity;


        $toPhoto = '/image';
        if ($request->has('product_image_1')) {
            File::delete(public_path($product->product_image_1));
            $image1 = $request->file('product_image_1');
            $namePhoto1 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_1.jpg";

            if ($image1->getSize() > $this->maxSize) {
                return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
            }

            $image1->move(public_path($toPhoto), $namePhoto1);

            $product->product_image_1 = $toPhoto . '/' . $namePhoto1;
        }

        if ($request->has('product_image_2')) {
            File::delete(public_path($product->product_image_2));
            $image2 = $request->file('product_image_2');
            $namePhoto2 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_2.jpg";

            if ($image2->getSize() > $this->maxSize) {
                return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
            }

            $image2->move(public_path($toPhoto), $namePhoto2);

            $product->product_image_2 = $toPhoto . '/' . $namePhoto2;
        }

        if ($request->has('product_image_3')) {
            File::delete(public_path($product->product_image_3));
            $image3 = $request->file('product_image_3');
            $namePhoto3 = time() . "_" . strtolower(str_replace(" ", "_", $request->product_name)) . "_3.jpg";

            if ($image3->getSize() > $this->maxSize) {
                return redirect()->back()->with('failed', 'Ukuran foto terlalu besar!');
            }

            $image3->move(public_path($toPhoto), $namePhoto3);

            $product->product_image_3 = $toPhoto . '/' . $namePhoto3;
        }

        $product->save();
        return redirect()->back()->with('success', 'Berhasil mengubah produk' . $product->product_name);
    }

    public function deleteProduct($id)
    {

        $product = Product::find($id);
        File::delete(public_path($product->product_image_1));
        File::delete(public_path($product->product_image_2));
        File::delete(public_path($product->product_image_3));
        $product->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus produk ' . $product->product_name);
    }

    public function changeStock(Request $request)
    {
        $product = Product::find($request->product_id);
        $product->product_quantity = $request->product_quantity;
        $product->save();

        return redirect()->back()->with('success', 'Berhasil mengubah stok produk' . $product->product_name);
    }

    public function order()
    {
        $detailOrders = DB::select("SELECT * FROM orders a  inner join customers d on a.customer_id = d.customer_id inner join tripay_channels e on a.channel_id = e.channel_id order by a.order_date asc");
        return view('staff/order', ['detailOrders' => $detailOrders]);
    }

    public function checkPayment($invoice)
    {
        $order = Order::where('invoice', $invoice)->first();
        $checkTransaction = $this->apiController->getTransactionTripay($order);

        switch ($checkTransaction->data->status) {
            case 'PAID':
                $this->apiController->addToSummary($order);
                $order->status = 'payed';
                break;
            case 'EXPIRED':
                $order->status = 'fail_pay';
                break;
            case 'FAILED':
                $order->status = 'fail_pay';
                break;
            default:
                break;
        }

        $order->save();

        return redirect()->back();
    }
}
